<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listBarang = DB::table('barang')->where('stok_barang','>',0)
        ->where('harga_barang','>',0)->get();

        return response()->json($listBarang, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #SETTING VALIDASI
        $input = $request->all();
        $validator = Validator::make($input, [
            'kode_barang' => 'required',
            'nama_barang' => 'required'
        ]);
        #RETURN VALIDATOR
        if($validator->fails())
        {
            $messages = $validator->messages();
            return response()->json($messages, 400);
        }

        $kode_barang = $request->input('kode_barang');
        $nama_barang = $request->input('nama_barang');
        DB::table('barang')->insert(['kode_barang'=>$request->input('kode_barang'),
    	    'nama_barang'=>$nama_barang]);

        return response()->json('Store Barang berhasil di proses',200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = DB::table('barang')->where('id', $id)->first();

        return response()->json($detail,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = DB::table('barang')->where('id', $id)->first();

        return response()->json($detail,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #SETTING VALIDASI
        $input = $request->all();
        $validator = Validator::make($input, [
            'kode_barang' => 'required',
            'nama_barang' => 'required'
        ]);
        #RETURN VALIDATOR
        if($validator->fails())
        {
            $messages = $validator->messages();
            return response()->json($messages, 400);
        }

        $kode_barang = $request->input('kode_barang');
        $nama_barang = $request->input('nama_barang');
        DB::table('barang')->where('id',$id)
        ->update(['kode_barang'=>$request->input('kode_barang'),
    	    'nama_barang'=>$nama_barang]);

        return response()->json('Update data barang berhasil dilakukan', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        #SOFT DELETE
        DB::table('barang')->where('id',$id)
        ->update(['deleted_at'=> date('Y-m-d')]);

        #HARD DELETE
        #DB::table('barang')->where('id',$id)->deleted();


        return response()->json('Data barang berhasil dihapus', 200);
    }

    #CUSTOM FUNCTION
    public function updateByCode(Request $request)
    {
        #setting
        $input = $request->all();
        $validator = Validator::make($input, [
            'code' => 'required',
            'name' => 'required'
        ]);
        #RETURN VALIDATOR
        if($validator->fails())
        {
            $messages = $validator->messages();
            return response()->json(['message' => 'error', 'data' => $messages ], 400);            
        }

        #QUERY BUILDER
        DB::table('barang')->where('code', $request->input('code'))->update(['name'=>$request->input('name')]);

        return response()->json(['message' => 'data berhasil', 'data' => 'berhasil diupdate' ], 201);
    }
}
