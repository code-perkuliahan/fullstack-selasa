<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;


class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listBarang = DB::table('barang')->where('stok','>',0)
        ->where('harga_barang','>',0)->all();

        return view('listbarang',compact('listBarang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('formbarang');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #SETTING VALIDASI
        $input = $request->all();
        $validator = Validator::make($input, [
            'kode_barang' => 'required',
            'nama_barang' => 'required'
        ]);
        #RETURN VALIDATOR
        if($validator->fails())
        {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($messages)->withInput($request->all());
        }

        $kode_barang = $request->input('kode_barang');
        $nama_barang = $request->input('nama_barang');
        DB::table('barang')->insert(['kode_barang'=>$request->input('kode_barang'),
    	    'nama_barang'=>$nama_barang]);

        return redirect('master/barang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = DB::table('barang')->where('id', $id)->first();

        return view('viewbarang', compact('detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = DB::table('barang')->where('id', $id)->first();

        return view('updatebarang', compact('detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #SETTING VALIDASI
        $input = $request->all();
        $validator = Validator::make($input, [
            'kode_barang' => 'required',
            'nama_barang' => 'required'
        ]);
        #RETURN VALIDATOR
        if($validator->fails())
        {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($messages)->withInput($request->all());
        }

        $kode_barang = $request->input('kode_barang');
        $nama_barang = $request->input('nama_barang');
        DB::table('barang')->where('id',$id)
        ->update(['kode_barang'=>$request->input('kode_barang'),
    	    'nama_barang'=>$nama_barang]);

        return redirect('master/barang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        #SOFT DELETE
        DB::table('barang')->where('id',$id)
        ->update(['deleted_at'=> date('Y-m-d')]);

        #HARD DELETE
        DB::table('barang')->where('id',$id)->deleted();


        return redirect('master/barang');
    }
}
