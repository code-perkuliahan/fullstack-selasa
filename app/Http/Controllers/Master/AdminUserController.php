<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AdminUserController extends Controller
{
    public function dataIndex()
    {
        #ELOQUENT
        $totalBerat = DB::table('pasien_anak_dasawismas')->select('id')
            ->where('berat_badan_anak','>=',20)->whereBetween('tinggi_badan_anak',[40, 70])
            ->sum('berat_badan_anak');

        #ELOQUENT KOMBINASI RAW
        $rerataBerat = DB::table('pasien_anak_dasawismas')->select(DB::Raw('avg(berat_badan_anak) as rerataBerat'))
            ->where('berat_badan_anak','>=',20)->whereBetween('tinggi_badan_anak',[40, 70])->first();

        
        $getList = DB::table('pasien_anak_dasawismas')->where(function($q){
            $q->where('nama_pasien_anak','like','%kode%')->orWhere('nama_pasien_anak','like','%kode%');
        })->where('berat_badan_anak','>=',20)->whereBetween('tinggi_badan_anak',[40, 70])->limit(20)->get();

        return view('listdata',compact('getList', 'totalBerat','rerataBerat'));

    }
    
}
